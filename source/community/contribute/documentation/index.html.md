---
layout: markdown_page
title: "Contributing to Documentation"
---

## Documentation

This section pertains to documentation changes that are independent of other code or feature changes. For documentation changes that accompany changes to code or features, see the [Contributing to Development page](/community/contribute/development/index.html).

See the [Documentation Styleguide](https://docs.gitlab.com/ee/development/documentation/styleguide.html) and [Writing Documentation](https://docs.gitlab.com/ee/development/documentation/) pages for more important details on writing documentation for GitLab.

1. Visit [docs.gitlab.com](https://docs.gitlab.com) for the latest documentation on GitLab, Charts, Omnibus, Runner and others.
1. Find a page that’s lacking important information or that has a spelling/grammar mistake. NOTE: Gitlab uses [US English](/handbook/communication/#writing-style-guidelines).
1. Click the “Edit this page” link at the bottom of the page, fork the relevant project, and modify the documentation in GitLab’s web editor. Alternatively, you can fork the relevant project locally and edit the corresponding file(s) in its `/doc` or `/docs` path.
1. Open a merge request and remember to follow [branch naming conventions](https://docs.gitlab.com/ee/development/documentation/index.html#branch-naming) if you forked the project locally.
1. Mention `@gl-docsteam` in a comment, then wait for a review. You may need to change things if a reviewer requests it.
1. Get your changes merged!

For those interested in writing full technical articles, we also have a [GitLab Community Writers Program](/community/writers/) which includes compensation.

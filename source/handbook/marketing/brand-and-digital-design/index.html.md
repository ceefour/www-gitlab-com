---
layout: handbook-page-toc
title: "Brand and Digital Design Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Digital Department


---

# Mission  

We build deliverables and experiences in both digital and physical realms such as the about.gitlab.com website, brand collateral, and more.

We assist with conversion design, growth marketing, user experience (UX), search-engine optimization (SEO), and performance metrics to build the logged out experience. We're involved with related marketing campaigns, events, lead-generation, and more. We guide the brand design and experience to assist with creating a cohesive identity.

## Lean Design

### Metrics & Measures
[ TODO : Document ]

## Value-Focused

### Objectives
[ TODO : Document ]

### Design is Good Business
[ TODO : Document ]
t
## Co-Design

### Inclusive
[ TODO : Document ]

---

# Workflow
## Prioritize

### Value-focused
[ TODO : Document ]

### Metrics
[ TODO : Document ]

#### Periscope Dashboards
[ TODO : Document ]

## MVC

### Product Performance Indicators
[ TODO : Document ]

### Function Team Refinement
[ TODO : Document ]

### Digital Board
[ TODO : Document ]

## Iterative Co-Design

### Design Systems


We've broken out the GitLab interface into a set of atomic pieces to form our design system, [Pajamas](https://design.gitlab.com/). Pajamas includes information such as our principles, components, usage guidelines, research methodologies, and more.

#### [GitLab Product UX Guide](https://docs.gitlab.com/ee/development/ux_guide/)

The goal of this guide is to provide written standards, principles and in-depth information to design beautiful and effective GitLab features. This is a living document and will be updated and expanded as we iterate.

## Feedback & Validation

#### AB Testing
[ TODO : Document ]
---

# Contribute

## Submit Early

Please submit your request early to allow for our team to plan accordingly. The earlier the better.

### Website

#### Prioritization

We use the following criteria to assess issue priority:

**Common**

* Frequency of use - How often is the single application diagram used? Web traffic to this page?
* Number of people impacted - How many GitLabbers would benefit from this asset? How many unique users would benefit from this page?

**Critical**

* High customer risk - If we don't do this, what are the risks to customers? How severe are those risks?
* High business risk - If we don't do this, how might it create risk for our business? Could it create a large volume of support calls? Make us non-GDPR compliant?
* Business criticality - Part of high ROI opportunity or other business critical initiative?
* Impact to important stakeholders - CEO or CMO request? Impacts bottom of funnel (BOFU) prospects very close to buying? Impacts key partners or customers?

**Differentiator**

* Brand and or product differentiator - Creates value by positioning our brand and or product against competition.

**Reusable**

* Can we reuse - If we build this, can we reuse it elsewhere to get more ROI. Perhaps it's low value score for this project, but high "lifetime" value via reuse.

**Time & Cost**

* Time and cost required to complete the work.

**Deadlines**

* Are there any hard deadlines due to contract or event obligations?

#### Issue Templates

Because the website exists as a project at the top level, labels and boards for the digital team should usually be created in the root gitlab.com group. For more information on this works, please see [How it all fits together](https://about.gitlab.com/handbook/marketing/#how-it-all-fits-together).

* [Website bug report](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report)
* [Website work request](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-work-request)

#### Labels

**At a minimum, website related issues should have the label `mktg-website` applied in order to populate appropriate boards.**

They should also have a label for your team and/or subject matter (ex: `blog`, `Digital Marketing`, `SEO`). These labels need to exist in either the root `GitLab.com` group or the `www-gitlab-com` repository.

Issues should follow the [standard marketing status labels](/handbook/marketing/#boards-and-labels) flow labels, with a few additions.

* `mktg-status::triage` this work is in the pre-planning stage. We're still discussing what to do.
* `mktg-status::plan` this work is in the planning stage. We know what we want to do but don't know how we want to do it yet.
* `mktg-status::design` this issue needs some prototyping or other UX designs before we know where we're going.
* `mktg-status::ready-to-build` this issue has been planned and detailed. We know what needs to be done. We can start building it.
* `mktg-status::wip` this issue is actively being worked on.
* `mktg-status::blocked` something is blocking progress on this issue.
* `mktg-status::review` enough work has been completed that this is ready for review and approval.
* `mktg-status::scheduled` this issue cannot be merged until a scheduled date but the work is complete and approved.

**Issues with an immovable due-date because of contractual and/or meatspace obligations should apply the `hard-deadline` label.**

Examples of optional labels include:

* `OKR`
* `outsource`

#### Issue Boards

**General**

- [Design Handbook](https://gitlab.com/groups/gitlab-com/-/boards/1498563): This board shows the status of all mktg-website issues labeled design-handbook. These are issues relating to team documentation and NOT general issues with the handbook.

**Brand Design**

- [Design - Priority](https://gitlab.com/groups/gitlab-com/-/boards/1511332): This board shows the status of all Brand Design issues labeled with a design priority label.
- [Design - Outsource](https://gitlab.com/groups/gitlab-com/-/boards/1511334): This board shows the status of all outsourcable issues labeled with design.

**Digital Design**

- [Blocked](https://gitlab.com/groups/gitlab-com/-/boards/1485169): This board shows all mktg-website website issues with the mktg-status::blocked label.
- [CMO](https://gitlab.com/groups/gitlab-com/-/boards/1486533): This board shows the status of all mktg-website issues labeled mktg-website which have the CMO Attention label.
- [Debt](https://gitlab.com/groups/gitlab-com/-/boards/1485186): This board shows the status of all mktg-website issues labeled technical-debt.
- [OKR](https://gitlab.com/groups/gitlab-com/-/boards/1483333): This board shows the status of all mktg-website issues labeled OKR.
- [Mktg Web - Priority](https://gitlab.com/groups/gitlab-com/-/boards/1483370): This board shows the status of all mktg-website issues labeled with a design priority label.
- [Marketing Web - Outsource](https://gitlab.com/groups/gitlab-com/-/boards/1502288): This board shows the status of all outsourcable issues labeled mktg-website.
- [Team Dev](https://gitlab.com/groups/gitlab-com/-/boards/1485124): This board shows all issues assigned to a member of the marketing website design & delivery teams. It is recommended to filter this by a particular mktg-status:: label such as ::wip or ::ready-to-build.
- [Hard Deadlines](https://gitlab.com/groups/gitlab-com/-/boards/1485208): This board shows the status of all mktg-website issues labeled hard-deadline (please refer to the definition on the label).

#### Team-subject boards

**Digital Design**

* [All Remote](https://gitlab.com/groups/gitlab-com/-/boards/1485066)
* [Analyst Relations](https://gitlab.com/groups/gitlab-com/-/boards/1485071)
* [Blog](https://gitlab.com/groups/gitlab-com/-/boards/1483337)
* [Content Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1483354)
* [Corporate Events](https://gitlab.com/groups/gitlab-com/-/boards/1485090)
* [Corporate Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485085)
* [Digital Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485086)
* [Field Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1533790)
* [Marketing Ops](https://gitlab.com/groups/gitlab-com/-/boards/1485111)
* [Marketing Programs](https://gitlab.com/groups/gitlab-com/-/boards/1580521)
* [Product Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1485129)
* [Recruiting (Talent Brand)](https://gitlab.com/groups/gitlab-com/-/boards/1580510)
* [Strategic Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1489415)
* [Social](https://gitlab.com/groups/gitlab-com/-/boards/1485179)
* [Talent Brand](https://gitlab.com/groups/gitlab-com/-/boards/1580510)
* [Technical Evangelism](https://gitlab.com/groups/gitlab-com/-/boards/1485182)

**Brand Design**

* [All Remote](https://gitlab.com/groups/gitlab-com/-/boards/1571555)
* [Blog](https://gitlab.com/groups/gitlab-com/-/boards/1571570)
* [Content Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1571561)
* [Corporate Events](https://gitlab.com/groups/gitlab-com/-/boards/1541174)
* [Corporate Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1541149)
* [Digital Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1571582)
* [Field Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1541162)
* [Marketing Programs](https://gitlab.com/groups/gitlab-com/-/boards/1571580)
* [Recruiting (Talent Brand)](https://gitlab.com/groups/gitlab-com/-/boards/1571573)
* [Strategic Marketing](https://gitlab.com/groups/gitlab-com/-/boards/1571417)
* [Social](https://gitlab.com/groups/gitlab-com/-/boards/1571585)
* [Technical Evangelism](https://gitlab.com/groups/gitlab-com/-/boards/1596489)

#### Tools

- [Adobe Creative Cloud / Suite](https://www.adobe.com/): Adobe Creative Cloud is a set of applications and services from Adobe Inc. that gives subscribers access to a collection of software used for graphic design, video editing, web development, photography, along with a set of mobile applications and also some optional cloud services.
- [Sketch](https://www.sketch.com/): Create, prototype, collaborate and turn your ideas into incredible products with the definitive platform for digital design.
- [Mural](https://mural.co/): MURAL is an Online Virtual Collaboration Space, Easy to Use Specially Designed for Teams. You can Post Stickies, Share Ideas, Brainstorm and Run Product Sprints.
- [Google Analytics](https://analytics.google.com/analytics/web/): Google Analytics lets you measure your advertising ROI as well as track your Flash, video, and social networking sites and applications.
- [Sisense ( previously Periscope )](https://www.sisense.com/product/data-teams/): Sisense for Cloud Data Teams (previously Periscope Data) empowers data teams to quickly connect to cloud data sources, then explore and analyze data in a matter of minutes. Extend cloud investments with the Sisense analytics platform to build, embed, and deploy analytics at scale.
- [Hotjar](https://www.hotjar.com/): Hotjar is a powerful tool that reveals the online behavior and voice of your users. By combining both Analysis and Feedback tools, Hotjar gives you the ‘big picture’ of how to improve your site's user experience and performance/conversion rates.
- [Launch Darkly](https://launchdarkly.com/): LaunchDarkly is a Feature Management Platform that serves over 100 billion feature flags daily to help software teams build better software, faster.
- [Google Optimize](https://optimize.google.com/optimize/home/): Google Website Optimizer was a free website optimization tool that helped online marketers and webmasters increase visitor conversion rates and overall visitor satisfaction by continually testing different combinations of website content.

#### Experiments

This section is related to A/B and multivariate testing on the marketing website, about.gitlab.com. It is a work in progress while we assess new testing tools for integration into our toolkit.

Until the toolkit assessment is finalized, please reference digital marketing's [testing documentation](https://about.gitlab.com/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#create-a-culture-of-testing-and-optimization).

Going forward, we hope newly created issues align with the [growth team's testing template](https://gitlab.com/gitlab-org/growth/product/-/blob/master/.gitlab/issue_templates/Growth%20experiment.md).

**Before you experiment**

Always gather relevant heatmaps, analytics, metrics, KPI (key performance indicators), etc.

**Testing Tools**

We are in the process of establishing a new toolset for running experiments. Our hybrid suite of tools will include:

*Testing via feature flags*

This is where we plan to do the bulk of our testing. We can run several of these at the same time. For full-page, partial-page, component, and small changes.

*Feature flag best practices*

Feature flags should be implemented in code similarly to the includes system. Example:

* Login to our third party service and create a feature flag and related configuration variables.
* Assign ownership of that flag from within the interface.
* Edit a page.
* Put the existing contents of the page into an include file named `/source/experiments/1234-control.html.haml`, where experiments is the folder name instead of includes and 1234 is the id number of the associated issue. "Control" refers to the baseline measurement you are testing against.
* Duplicate that include file with the name `/source/experiments/1234-test.html.haml`
* Make your changes and validate the feature toggle works locally and/or on a review app before deployment.
* Ensure you'll be able to collect all the data you need. Setup appropriate tools like heatmaps and analytics.
* Note that one advantage of feature flags is that they can be released to production without being turned on.
* When ready, enable the test. Start gathering data.

**Testing via CDN**

This is an advanced tool meant to test large-scale changes at a systemic level. For now we plan to run only one of these at a time.

**Testing via WYSIWYG**

This is a rudimentary tool for small-scale changes with few safeguards and important caveats. We can use this for small items like colors and copy but not layout. This is mainly meant as a tool for non-developers.

### Brand

#### Issue Templates

* [Brand and Digital Design team grooming](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming)
* [General Design request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-general)
* [Content Resource Design request](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-request-content-resource)
* [Integrated Campaign Design requirements](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=design-requirements-integrated-campaign)

#### Labels

The `Design` label helps us find and track issues relevant to the Design team. If you create an issue where Design is the primary focus, please use this label.

#### Cross-link to design system
[ TODO : Document ]

---

# Brand Guidelines  

## Home

### How we work
[ TODO : Document ]

#### Partnership with third parties

In certain cases, the help of a third party agency or design partner may be brought in for a project. The following serves as criteria for when to outsource design:

- Smaller-scale projects, such as stickers or requests that do not meet our current business prioritization, where the brand guidelines provide sufficient creative direction and parameters for the third party to work with. 
- Larger-scale projects where the Brand and Digital team need additional support given the timeline and/or scale of the request.

Whenever a third party is brought in to support design, the work must be shared with the Brand and Digital team to ensure brand integrity and that we are [working transparently](https://about.gitlab.com/handbook/values/#transparency) with one another.

For guidance on which third-party agency or designer(s) to work with, please reach out to [brand-and-digital@gitlab.com](mailto:brand-and-digital@gitlab.com).

#### Requesting design help

1. Create an [issue](https://about.gitlab.com/handbook/marketing/brand-and-digital-design/#issue-templates-2) in the corresponding project repository.
    1. For tasks pertaining to [about.gitlab.com](/) create an issue in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues).
    1. For all other marketing related tasks create an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues).
1. Add all relevant details, goal(s), purpose, resources, and links in the issue description. Also `@` mention team members who will be involved.
1. Set due date (if possible) — please leave at least 2 week lead time in order to generate custom design assets. If you need them sooner, ping @luke in the #marketing-design Slack channel and we will make our best effort to accommodate, but can't promise delivery.
1. Add the `Design` and `Website Redesign` (if applicable) label(s) to your issue.

#### Project prioritization

Per the Design team's discretion, the prioritization of design projects will be based on the direct impact on Marketing.

To get a better sense of [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) prioritization, you can view the [Design Issue Board](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/913023?&label_name[]=Design).

Design projects within the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com/issues) can be tracked using the [Website](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Website) label. The prioritization of projects for [about.gitlab.com](/) can be viewed on the [Website Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/boards/349137?milestone_title=No+Milestone&).

Any design requests that do not fall in line with the goals and objectives of Marketing will be given a lower priority and factored in as time allows.

#### Team logo request guidelines

As the company continues to grow, incoming requests for internal team logos are increasing at a rate that is not scalable for the Brand Design team. We understand the desire for teams within GitLab to have their own identity, so we've created these guidelines to help direct your request:

* Teams can create their own logos that are for internal (non-public) use only.
* If you believe a public-facing team logo would be valuable to our business, please submit a [design request issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) outlining how it will be used, who will see it, and it's perceived value. Logos will be created and approved on a case-by-case basis to capitalize on brand opportunities and ensure brand integrity.

### Privacy
[ TODO : Document ]

### License
[ TODO : Document ]

### What's new  
[ TODO : Document ]

## Design principles   
### Generate value  
[ TODO : Document ]

### Omni-channel experiences  
[ TODO : Document ]

### Everyone can contribute
[ TODO : Document ]

### Grow a community  
[ TODO : Document ]

## Foundations  

### Personality

GitLab's brand has a personality that is reflected in everything we do. It doesn't matter if we are hosting a fancy dinner with fortune 500 CIOs, at a hackathon, or telling our story on about.gitlab.com…across all our communication methods, and all our audiences, GitLab has a personality that shows up in how we communicate.

Our personality is built around four main characteristics.

- Human: We write like we talk. We avoid buzzwords and jargon, and instead communicate simply, clearly, and sincerely. We treat people with kindness.
- Competent: We are highly accomplished, and we communicate with conviction. We are efficient at everything we do.
- Quirky: We embrace diversity of opinion. We embrace new ideas based on their merit, even if they defy commonly held norms.
- 
Humble: We care about helping those around us achieve great things more than we care about our personal accomplishments.

These four characteristics work together to form a personality that is authentic to GitLab team-members, community, and relatable to our audience. If we were quirky without being human we could come across as eccentric. If we were competent without being humble we could come across as arrogant.

GitLab has a [higher purpose](https://about.gitlab.com/company/strategy/#mission). We want to inspire a sense of adventure in those around us so that they join us in contributing to making that mission a reality.

### Writing style

The following guide outlines the set of standards used for all written company communications to ensure consistency in voice, style, and personality, across all of GitLab's public communications.

See the [Blog Editorial Style Guide](https://about.gitlab.com/handbook/marketing/corporate-marketing/content/editorial-team/#blog-style-guide) for more.

The [tone of voice](https://about.gitlab.com/handbook/marketing/corporate-marketing/#tone-of-voice-1) we use when speaking as GitLab should always be informed by our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy). 

### Website

#### Using other logos

Logos used on the about.gitlab.com site should always be in full color and be used to the specifications provided by the owner of that logo, which can usually be found on the owners website. The trust marks component found throughout the site is the only exception and should use a neutral tone:

<img src="/images/handbook/marketing/corporate-marketing/design/trust-marks.png" class="full-width">

#### Text

Our website uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family. Headers (h1, h2, etc.) always have a weight of 600 (unless used in special situations like large, custom quotes) and the body text always has a weight of 400. Headers should not be given custom classes, they should be used as tags and tags alone (h1, h2, etc.) and their sizes or weights should not be changed, unless rare circumstances occur. Here are typography tags.

`H1: Header Level 1`

`H2: Header Level 2`

`H3: Header Level 3`

`H4: Header Level 4`

`p: Body text`

#### Buttons

Buttons are an important facet to any design system. Buttons define a call to action that lead people somewhere else, related to adjacent content. Here are buttons and their classes that should be used throughout the marketing website:

**Note**: Text within buttons should be concise, containing no more than 4 words, and should not contain bold text. This is to keep things simple, straightforward, and limits confusion as to where the button takes you.

**Primary buttons**

Primary buttons are solid and should be the default buttons used. Depending on the color scheme of the content, purple or orange solid buttons can be used depending on the background color of the content. These primary buttons should be used on white or lighter gray backgrounds or any background that has a high contrast with the button color. They should also be a `%a` tag so it can be linked elsewhere and for accessibility. Buttons should also be given the class `margin-top20` if the button lacks space between itself and the content above.

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.orange</pre>
  <p>OR</p>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple">Primary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.purple</pre>
</div>

**Secondary Buttons**

There will be times when two buttons are needed. This will be in places such as [our jobs page](/jobs/), where we have a button to view opportunities and one to view our culture video. In this example, both buttons are solid, but one is considered the primary button (orange), and the other is the secondary button (white). The CSS class for the solid white button is <br> `.btn.cta-btn.btn-white`.

<img src="/images/handbook/marketing/corporate-marketing/design/jobs-buttons-example.png" class="full-width">

This is the proper use of two buttons, both being solid, but different colors based on hierarchy. If the background is white or a lighter color that doesn't contrast well with a white-backgound button, a ghost button should be used as a secondary button, and should match in color to the primary button beside it as shown below:

<div class="buttons-container flex-start">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn orange margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button</a>
</div>

<div class="buttons-container flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn purple margin-top20">Primary Button</a>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple margin-top20">Secondary Button</a>
</div>

DO NOT: Do not use these ghost buttons styles as standalone buttons. They have been proven to be less effective than solid buttons [in a number of studies](https://conversionxl.com/blog/ghost-buttons/). They should only be used as a secondary button, next to a solid primary button that already exists. Here are the classes for the secondary buttons:

<div class="flex-container flex-column flex-start margin-bottom20">
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-orange margin-top20">Secondary Button 1</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-orange</pre>
  <a href="/handbook/marketing/corporate-marketing/#primary-buttons" class="btn cta-btn ghost-purple">Secondary Button 2</a>
  <pre class="highlight shell">.btn.cta-btn.ghost-purple</pre>
</div>

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. The GitLab iconography currently consists of "label icons" and "content icons", each are explained in further detail below:

**Label icons**

Label icons are intended to support usability and interaction. These are found in interactive elements of the website such as navigation and [toggles](/pricing/).

![Label icons example](/images/handbook/marketing/corporate-marketing/design/label-icons-example.png){: .medium.center}

**Content icons**

Content icons are intended to provide visual context and support to content on a webpage; these icons also have a direct correlation to our illustration style with the use of bold outlines and fill colors.

A few examples include our [event landing pages](/events/aws-reinvent/) and [Resources page](/resources/).

<img src="/images/handbook/marketing/corporate-marketing/design/content-icons-example.png" class="full-width">

### Logos

To download the GitLab logo (in various formats and file types) check out our [Press page](/press/).

The GitLab logo consists of two components, the icon (the tanuki) and the wordmark:

![GitLab logo](/images/handbook/marketing/corporate-marketing/design/gitlab-lockup.png){: .small.left}

GitLab is most commonly represented by the logo, and in some cases, the icon alone. GitLab is rarely represented by the wordmark alone as we'd like to build brand recognition of the icon alone (e.g. the Nike swoosh), and doing so by pairing it with the GitLab wordmark.

#### Logo safe space

Safe space acts as a buffer between the logo or icon and other visual components, including text. this space is the minimum distance needed and is equal to the x-height of the GitLab wordmark:

![Logo x-height](/images/handbook/marketing/corporate-marketing/design/x-height.png){: .medium.left}

![Logo safe space](/images/handbook/marketing/corporate-marketing/design/logo-safe-space.png){: .small.left}

![Icon safe space](/images/handbook/marketing/corporate-marketing/design/icon-safe-space.png){: .small.left}

The x-height also determines the proper spacing between icon and wordmark, as well as, the correct scale of the icon relative to the wordmark:

![Stacked logo safe space](/images/handbook/marketing/corporate-marketing/design/stacked-logo-safe-space.png){: .small.left}

#### Minimum logo size

Here are the recommended minimum sizes at which the logo may be reproduced. For legibility reasons, we ask that you stick to these dimensions:

**Logo**

![Horizontal logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-min-size.png){: .small.left}

- Digital: 100px wide
- Print: 1.25in (31.75mm) wide

**Stacked logo**

![Stacked logo minimum size](/images/handbook/marketing/corporate-marketing/design/logo-stacked-min-size.png){: .small.left}

- Digital: 60px wide
- Print: 0.75in (19mm) wide

**Icon**

![Icon minimum size](/images/handbook/marketing/corporate-marketing/design/icon-min-size.png){: .small.left}

- Digital: 30px wide
- Print: 0.50in (13mm) wide


#### The Tanuki

The [tanuki](https://en.wikipedia.org/wiki/Japanese_raccoon_dog) is a very smart animal that works together in a group to achieve a common goal. We feel this symbolism embodies GitLab's [mission](/company/strategy/#mission) that everyone can contribute, our [values](/handbook/values/), and our [open source stewardship](/company/stewardship/).

The tanuki logo should also not have facial features (eyes, ears, nose...); it is meant to be kept neutral, but it can be accessorized.

#### Brand oversight

Occasionally the [old GitLab logo](* https://gitlab.com/gitlab-com/gitlab-artwork/blob/master/_archive/logo/fox.png) is still in use on partner websites, diagrams or images, and within our own documentation. If you come across our old logo in use, please bring it to our attention by creating an issue in the [Marketing](https://gitlab.com/gitlab-com/marketing/general/issues) issue tracker. Please include a link and screenshot (if possible) in the description of the issue and we will follow-up to get it updated. Thanks for contributing to our brand integrity!

### Trademark

GitLab is a registered trademark of GitLab, Inc. You are welcome to use the GitLab trademark and logo, subject to the terms of the [Creative Commons Attribution Non-Commercial ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/). The most current version of the GitLab logo can be found on our [Press page](/press/).

Under the Creative Commons license, you may use the GitLab trademark and logo so long as you give attribution to GitLab and provide a link to the license. If you make any changes to the logo, you must state so, along with the attribution, and distribute under the same license.

Your use of the GitLab trademark and logo:

- May not be for commercial purposes;
- May not suggest or imply that you or your use of the GitLab trademark or logo is endorsed by GitLab, or create confusion as to whether or not you or your use of the GitLab trademark or logo is endorsed by GitLab; and
- May not suggest or imply or that you are affiliated with GitLab in any way, or create confusion as to whether or not you are affiliated with GitLab in any way.

Examples of improper use of the GitLab trademark and logo:

- The GitLab name may not be used in any root URL, including subdomains such as `gitlab.company.com` or `gitlab.citool.io`.
- The GitLab trademark and/or logo may not be used as the primary or prominent feature on any non-GitLab materials.

### Typography  

The GitLab brand uses the [Source Sans Pro](https://fonts.google.com/specimen/Source+Sans+Pro?selection.family=Source+Sans+Pro) font family.

### Color

While the brand is ever-evolving, the GitLab brand currently consists of six primary colors that are used in a wide array of marketing materials. RGB and CMYK swatch libraries can be found [here](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/tree/master/design/gitlab-brand-files/color-palettes). 

##### Hex/RGB

![GitLab Hex/RGB Colors](/images/handbook/marketing/corporate-marketing/design/gitlab-hex-rgb-colors.png)


### Illustration  

### Iconography

Icons are a valuable visual component to the GitLab brand; contributing to the overall visual language and user experience of a webpage, advertisement, or slide deck. 

**Badges**
[TODO: Document]

**Patterns**
[TODO: Document]

#### Illustration library
[ TODO : Document ]


#### Icon library

- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/line-icons) (.png)
- [Line icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/_resources/icons/svg) (.svg)
- [Icon illustrations](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/presentation-decks/_general-assets/icons/illustrated-icons) (.png)
- [Software Development Lifecycle](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/software-development-lifecycle/complete-lifecycle-icons/png)

#### Icon pattern

- [GitLab icon pattern](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/illustrations/icon-pattern)

#### Social media

- [Profile assets](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/social-media/profile-assets/png/assets)

### Photography

#### Photo library
[ TODO : Document ]

### Brand resources

- [GitLab icons](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-logo) (web RGB & print CMYK)
- [GitLab logos](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/gitlab-wordmark) (web RGB & print CMYK)
- Print-ready [event one-pagers](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/print/event-one-pagers)
- Color palette and Typography can be found in the [Brand Guidelines](#brand-guidelines)
- [Authorized Reseller GitLab Virtuoso Badge](https://gitlab.com/gitlab-com/marketing/general/tree/master/design/gitlab-brand-files/authorized-resellers/gitlab-virtuoso-badge)

### Templates

#### Issue templates

To work more efficiently as an [asynchronous](/company/culture/all-remote/asynchronous/) team, everything should [begin](/handbook/communication/#everything-starts-with-a-merge-request) in an issue or merge request as opposed to Slack or email.

Creating an issue may seem like an added burden, but the long-term benefit of having [context](/company/culture/all-remote/effective-communication/#understanding-low-context-communication) around a given piece of work prevents [knowledge gaps](https://about.gitlab.com/company/culture/all-remote/asynchronous/#plugging-the-knowledge-leak) from occurring. Issue templates exist to make the process of creating issues easier. If you find yourself starting similar issues over and over, look through existing issue templates. If a suitable one does not exist, consider creating one in the appropriate repository.

For more on how to make beautiful templates, check out GitLab's [Markdown Guide](https://about.gitlab.com/handbook/engineering/ux/technical-writing/markdown-guide/). To add an emoji in an issue, begin by typing `:` and the title of the emoji. A list of emoji markup is [here](https://gist.github.com/rxaviers/7360908).

Remember to always add `Related Issues` and `Epics` after you've created your issue so others have context on what issues connect to this work.

- Visit the [Corporate Marketing Issue Template Repository](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/tree/master/.gitlab/issue_templates) to view all available issue templates. You'll find templates covering [Corporate Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/Corporate-Event-Request.md), [Bulk Swag Requests](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/BulkSwagRequest.md), Social Requests for [Events](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-event-request.md) and [General](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-general-request.md), Video Requests, [Webpage Updates](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/webpage-update.md), and more.
- If you're for a [general or universal issue template](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/general-project-template.md) to begin tracking discussion and progress on any given piece of work, big or small, search for `general-project-template` in a newly-created [Corporate Marketing Issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues). This template is pre-populated and beautified with emojis and descriptors for common sub-sections such as `Background`, `Details and reach`, `Goals and key messages`, and `Due dates, DRI(s) and next steps/to-dos`.


#### Presentation kits

- [General GitLab deck template](https://docs.google.com/a/gitlab.com/presentation/d/16FZd01-zCj_1jApDQI7TzQMFnW2EGwtQoRuX82dkOVs/edit?usp=sharing)
- [Group Conversation template](https://docs.google.com/a/gitlab.com/presentation/d/1JYHRhLaO9fMy1Sfr1WDnCPGv6DrlohlpOzs48VvmlQw/edit?usp=sharing)
- [GitLab pitch deck template](https://docs.google.com/a/gitlab.com/presentation/d/1LC1lT-gxpl1oUZ2InX4Oni9T4MfR0DFF0RLi4uNxBBQ/edit?usp=sharing)

#### Event kits
[ TODO : Document ]

#### Swag kit
[ TODO : Document ]

#### Print templates
[ TODO : Document ]

---

# Teams

## Brand  

### Overview  

The Design team has a rather wide reach and plays a big part in almost all marketing efforts. Design touchpoints range from the [GitLab website](/) to print collateral, swag, and business cards. This includes, but certainly not limited to:

#### Field Design & Branding
{:.no_toc}
- Marketing Design System
- Conference booth design
- Banners & signage
- Swag
- Event-specific slide decks
- Event-specific branding (e.g. GitLab World Tour, Team Summits, etc.)
- Business cards
- One-pagers, handouts, and other print collateral
- GitLab [Brand Guidelines](#brand-guidelines)

#### Content Design
{:.no_toc}
- Promotional videos & animations
- Social media campaign assets
- Webcast collateral & assets
- eBooks
- Whitepapers
- Infographics & diagrams

*In the spirit of 'everyone can contribute' (as well as version control and SEO) we prefer webpages over PDFs. We will implement a `print.css` component to these webpages so that print PDFs can still be utilized for events and in-person meetings without the headache of version control*

### Team  

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard



[**Luke Babb**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#luke)

* Title: Manager, Creative
* Email: luke@gitlab.com
* GitLab handle: @luke
* Slack handle: @luke



[**Monica Galletto**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#monica_galletto) 

* Title: Associate Production Designer
* Email: mgalletto@gitlab.com
* GitLab handle: @monica_galletto
* Slack handle: @monicagalletto



[**Vic Bell**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#vicbell)	

* Title: Senior Illustrator
* Email: vbell@gitlab.com
* GitLab handle: @vicbell
* Slack handle: @vic



#### Contact Us
* Slack: [#marketing-design](https://gitlab.slack.com/app_redirect?channel=marketing-brand-and-digital)
* Slack: [#marketing-website-team](https://gitlab.slack.com/app_redirect?channel=marketing-design)
* Email: [brand-and-digital@gitlab.com](mailto:brand-and-digital@gitlab.com)

## Digital

### Overview  

The Digital team is responsible for the development and maintence of about.gitlab.com and technical support for marketing campaigns. This includes, but certainly not limited to:

{:.no_toc}
- Redesign, updates, and maintenance of the [GitLab website](/)
- Landing pages (campaigns, webcasts, events, and feature marketing)
- Email template design

### Team

[**Shane Bouchard**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#sbouchard1)

* Title: Director of Brand and Digital Design
* Email: sbouchard@gitlab.com
* GitLab handle: sbouchard1
* Slack handle: sbouchard



[**Brandon Lyon**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#brandon_lyon)

* Title: Website Developer and Designer
* Email: skarpeles
* GitLab handle: @brandon_lyon
* Slack handle: @Brandon Lyon 



[**Lauren Barker**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#laurenbarker) 

* Title: Fullstack Engineer
* Email: lbarker@gitlab.com
* GitLab handle: laurenbarker
* Slack handle: lbarker



[**Stephen Karpeles**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#skarpeles)	

* Title: Website Developer and Designer
* Email: skarpeles@gitlab.com
* GitLab handle: @skarpeles
* Slack handle: @Stephen Karpeles


#### Handy Links

- [Blocked](https://gitlab.com/groups/gitlab-com/-/boards/1485169): This board shows all mktg-website website issues with the mktg-status::blocked label.
- [Bugs](https://gitlab.com/groups/gitlab-com/-/boards/1483331): This board shows the status of all mktg-website issues filed using the website bug template.
- [CMO](https://gitlab.com/groups/gitlab-com/-/boards/1486533): This board shows the status of all mktg-website issues labeled mktg-website which have the CMO Attention label.
- [Debt](https://gitlab.com/groups/gitlab-com/-/boards/1485186): This board shows the status of all mktg-website issues labeled technical-debt.
- [OKR](https://gitlab.com/groups/gitlab-com/-/boards/1483333): This board shows the status of all mktg-website issues labeled OKR.
- [Overall](https://gitlab.com/groups/gitlab-com/-/boards/1472883): This board shows the status of all appropriately labeled mktg-website issues.
- [Mktg Web - Priority](https://gitlab.com/groups/gitlab-com/-/boards/1483370): This board shows the status of all mktg-website issues labeled with a design priority label.
- [Marketing Web - Outsource](https://gitlab.com/groups/gitlab-com/-/boards/1502288): This board shows the status of all outsourcable issues labeled mktg-website.
- [Team Dev](https://gitlab.com/groups/gitlab-com/-/boards/1485124): This board shows all issues assigned to a member of the marketing website design & delivery teams. It is recommended to filter this by a particular mktg-status:: label such as ::wip or ::ready-to-build.
- [Hard Deadlines](https://gitlab.com/groups/gitlab-com/-/boards/1485208): This board shows the status of all mktg-website issues labeled hard-deadline (please refer to the definition on the label).	

#### Contact Us
* Slack: [#marketing-brand-and-digital](https://gitlab.slack.com/app_redirect?channel=marketing-brand-and-digital)
* Slack: [#marketing-website-team](https://gitlab.slack.com/app_redirect?channel=marketing-website-team)
* Email: [brand-and-digital@gitlab.com](mailto:brand-and-digital@gitlab.com)

## Growth  

### Overview  
[ TODO : Document ]

### Team  
[ TODO : Document ]

### Handy Links
[ TODO : Document ]

#### Homepage merchandising schedule & other revelant calendars
[ TODO : Document ]

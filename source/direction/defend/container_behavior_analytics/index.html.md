---
layout: markdown_page
title: "Category Direction - Container Behavior Analytics"
---

- TOC
{:toc}

## Defend

| | |
| --- | --- |
| Stage | [Defend](/direction/defend) |
| Maturity | [Planned](/direction/maturity/) |
| Content Last Reviewed | `2020-03-11` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Container Behavior Analytics in GitLab. This page belongs to the Container Security group of the Defend stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/issues/31901) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/744) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for container security, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Container Behavior Analytics (CBA) refers to the ability to detect, report, and respond to attacks on containerized infrastructure and workloads. Techniques include use of one or more types of intrusion detection systems (IDS) to detect attacks.  The IDS may be supplemented with custom-built monitoring capabilities and/or behavior analytics to improve the efficacy and scope of detected attacks.

An IDS is a device or software application that monitors a network or systems for malicious activity or policy violations. Malicious activity can then be reported back to an Administrator either through GitLab or through a security information and event management (SIEM) system. IDS types range in scope from single computers to large networks. The most common classifications are network intrusion detection systems (NIDS) and host-based intrusion detection systems (HIDS). Some leverage honeypots to attract and characterize malicious traffic. Some strictly leverage signature-based detection, while others use machine learning to automatically detect anomalies.

An ideal Container Behavior Analytics solution would include all types of intrusion detection systems to provide defense-in-depth and protection against a wide range of attacks.  Additional analytics can be layered on top of the data collected from an IDS to help filter out false positives and to recommend new rules to reduce false negatives.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
TODO

#### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
TODO

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
We are planning to integrate an IDS into our product as a first step.  We will then integrate the IDS with other GitLab categories, such as the [Logging](/direction/monitor/apm/logging) category (to surface logs in GitLab) and the [Vulnerability Management](/direction/defend/vulnerability_management) category (to surface alerts in GitLab).

Longer-term we plan to add additional behavior analytics on top of our IDS to improve our threat detection capabilities.

#### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
We will start by [integrating the Falco IDS](https://gitlab.com/gitlab-org/gitlab/issues/31901) as the first step in this category. This provides some initial IDS capabilities that can be added to and refined in the future.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->
We are not currently planning to do the following:
*  Build our own SIEM
*  Provide both a NIDS and HIDS.  Assuming we integrate Falco as our IDS, we will primarily fall into the HIDS IDS categorization.

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
[Planned to Minimal](https://gitlab.com/groups/gitlab-org/-/epics/744)

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
TODO

### Why is this important?
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
TODO

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-management/process/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->
TODO

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->
TODO

### Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->
TODO

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->
TODO

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/values/#dogfooding)
the product.-->
TODO

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
We will need to integrate an IDS as an important first step toward our strategy.  Likely we will leverage the [Falco IDS](https://falco.org/).

Additional strategy items will be uncovered as we do more research in this area.

### Related Categories
<!-- What's the most important thing to move your strategy forward?-->
*  [UEBA](/direction/defend/ueba)
*  [Logging](/direction/monitor/apm/logging)
*  [Vulnerability Management](/direction/defend/vulnerability_management)

---
layout: markdown_page
title: "Category Direction - Load Testing"
---

- TOC
{:toc}

## Load Testing

Load testing is a common and typically important stage in the CI process for organizations of all sizes. We will help developers [shift left](https://stackify.com/shift-left-continuous-testing/) by making performance decisions earlier in the development process by providing a measure of how software responds under load as changes are introduced. 

- [Maturity Plan](https://gitlab.com/groups/gitlab-org/-/epics/952)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALoad%20Testing)
- [Overall Vision](/direction/ops/#verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1305) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

This page is maintained by the Product Manager for Testing, James Heimbuck ([E-mail](mailto:jheimbuck@gitlab.com))

## What's Next & Why

Up next is the Integrated Load Testing MVC ([gitlab-org#952](https://gitlab.com/groups/gitlab-org/-/epics/952)). This epic brings performance testing as a category to minimal maturity and provide a baseline experience on which we can learn more about the problems users face in load testing their software.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is "Minimal" (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Integrate k6 for load testing](https://gitlab.com/gitlab-org/gitlab/issues/10683)
- [Add integrated load testing to AutoDevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681)

## Competitive Landscape

### Azure DevOps

Azure DevOps offers [in-product load testing](https://docs.microsoft.com/en-us/azure/devops/test/load-test/get-started-simple-cloud-load-test?view=azure-devops).  This consists of different types of tests including:

* HTTP Archive Based tests
* URL based tests
* Apache JMeter Tests

For URL type tests, the output contains information about the average response time, user load, requests per second, failed requests and errors (if any).

### Travis CI/CircleCI

While, just as one could orchestrate any number of performance testing tools with GitLab CI today, Travis or CircleCI could be used to orchestrate performance testing tools, it does not have any built-in capabilities around this.

## Top Customer Success/Sales Issue(s)

TBD

## Top Customer Issue(s)

The most popular issue in this category is for the integration with k6 ([gitlb#10683](https://gitlab.com/gitlab-org/gitlab/issues/10683)).

## Top Internal Customer Issue(s)

Integrated Load Testing ([gitlab-org&952](https://gitlab.com/groups/gitlab-org/-/epics/952)) is the most sought after internal customer issue from Quality. Specifically, integrating with k6 ([gitlab#10683](https://gitlab.com/gitlab-org/gitlab/issues/10683)) has been asked for.

## Top Vision Item(s)
The top Vision item is [gitlab-ee#10681](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681) which will add Integrated Load Testing as an Auto DevOps step.  This will mean that what we have defined as minimal for this category will now be added to every Auto DevOps pipeline.

Enabling customers to specify additional URLs for testing ([gitlab#3540](https://gitlab.com/gitlab-org/gitlab/issues/3540)) and auto scaling load tests ([gitlab#35377](https://gitlab.com/gitlab-org/gitlab/issues/35377)) will also move our vision for load testing forward.

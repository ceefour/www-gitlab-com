#!/usr/bin/env ruby
# frozen_string_literal: true

require "uri"
require "gitlab"

require_relative "../../generators/releases"

namespace :release do
  GITLAB_FOSS_PROJECT_ID = 13083
  GITLAB_EE_PROJECT_ID = 278964

  CORE = "core"
  STARTER = "starter"
  PREMIUM = "premium"
  ULTIMATE = "ultimate"

  OTHER = "other"

  # These lists define the order of the features in the release notes.
  # Anything that doesn't have a matching tier or stage gets funnelled into the "other" bucket
  TIERS = [OTHER, ULTIMATE, PREMIUM, STARTER, CORE].freeze
  STAGES = %W[#{OTHER} manage plan create verify package secure release configure monitor defend].freeze

  namespace :ee do
    # gitlab-org/gitlab Releases page: https://gitlab.com/gitlab-org/gitlab/-/releases
    desc "Updates the gitlab-org/gitlab project's Releases page using the release posts found in this project"
    task :update_project_releases_page do |_t, args|
      update_project_releases_page(project_id: GITLAB_EE_PROJECT_ID, is_ee: true)
      puts "✅ Successfully updated all releases. You can view these releases at https://gitlab.com/gitlab-org/gitlab/-/releases."
    end
  end

  namespace :foss do
    # gitlab-org/gitlab-foss Releases page: https://gitlab.com/gitlab-org/gitlab-foss/-/releases
    desc "Updates the gitlab-org/gitlab-foss project's Releases page using the release posts found in this project"
    task :update_project_releases_page do |_t, args|
      update_project_releases_page(project_id: GITLAB_FOSS_PROJECT_ID, is_ee: false)
      puts "✅ Successfully updated all releases. You can view these releases at https://gitlab.com/gitlab-org/gitlab-foss/-/releases."
    end
  end

  def update_project_releases_page(project_id:, is_ee:)
    puts "Reading the releases posts..."
    release_posts = ReleaseList.new.release_posts
    puts "✅ Found #{release_posts.size} release posts"

    gitlab_client = Gitlab.client(endpoint: "https://gitlab.com/api/v4", private_token: ENV['GITLAB_BOT_TOKEN'])

    puts "Getting a list of all existing project Releases..."

    begin
      releases = gitlab_client.project_releases(project_id).auto_paginate
    rescue Gitlab::Error::Error => e
      warn("Failed to get the list of existing Releases: #{e.message}")
      abort
    end

    puts "✅ Retrieved #{releases.size} Releases from the API"

    release_posts.each do |rp|
      api_object = release_post_to_api_object(rp, is_ee: is_ee)

      tag_name = version_number_to_tag_name(rp.version, is_ee: is_ee)
      existing_release = releases.find { |r| tag_name == r.tag_name }

      if !existing_release
        puts "Creating a new Release for version #{rp.version}..."

        begin
          gitlab_client.create_project_release(project_id, api_object)
        rescue Gitlab::Error::Error => e
          warn("Failed to create a new Release: #{e.message}")
          abort
        end

        puts "✅ Successfully created a new Release for tag #{tag_name}"
      elsif existing_release.description != api_object[:description]
        # Only trigger an update if an existing Release was found _and_ the description is different

        puts "Updating Release for version #{rp.version}..."

        begin
          gitlab_client.update_project_release(project_id, tag_name, api_object)
        rescue Gitlab::Error::Error => e
          warn("Failed to update Release with tag #{tag_name}: #{e.message}")
          abort
        end

        puts "✅ Successfully updated existing Release for tag #{tag_name}"
      else
        puts "✅ Release for version #{rp.version} is already up-to-date"
      end
    end
  end

  # Converts a release post object into a hash that
  # can be used with the Release API (https://docs.gitlab.com/ee/api/releases)
  def release_post_to_api_object(release_post, is_ee:)
    # An array of strings that will be join()-ed together to form the release's description
    description = []

    # Defines the structure of the release post.
    # #### Top-level headings are tiers (Core, Starter, Premium, Ultimate)
    # ##### Next-level headings are stages (Manage, Plan, Create, etc.)
    # - List items are individual features ("highlights")
    tier_to_stages_map = Hash.new do |tier_to_stages_map, tier|
      tier_to_stages_map[tier] = Hash.new do |stage_to_highlights_map, stage|
        stage_to_highlights_map[stage] = []
      end
    end

    # Ignore features that don't target Core if we're working with FOSS
    filtered_highlights = release_post.highlights.filter do |highlight|
      is_ee || get_highlight_tier(highlight) == CORE
    end

    # Sort the highlights first by tier and then by stage, according
    # to the order of TIERS and STAGES at the top of this file
    sorted_highlights = filtered_highlights.sort_by.with_index do |highlight, index|
      [TIERS.index(get_highlight_tier(highlight)), STAGES.index(get_highlight_stage(highlight)), index]
    end

    # Place each release post into the appropriate place in the structure
    sorted_highlights.each do |hl|
      tier = get_highlight_tier(hl)
      stage = get_highlight_stage(hl)
      tier_to_stages_map[tier][stage] << hl
    end

    # Generate the GFM by walking through the structure created above
    tier_to_stages_map.each do |tier, stages|
      description.concat(render_tier(tier, stages, is_ee: is_ee))
    end

    {
      name: "GitLab #{release_post.version}",
      tag_name: version_number_to_tag_name(release_post.version, is_ee: is_ee),
      description: description.join("\n"),
      released_at: release_post.date.iso8601,
      assets: {
        links: [

          # Include a link to this release's blog post
          {
            name: "GitLab #{release_post.version} release post",
            url: "https://about.gitlab.com/releases/#{release_post.date.strftime('%Y/%m/%d')}/gitlab-#{release_post.version.tr('.', '-')}-released/"
          }
        ]
      }
    }
  end

  # Renders a tier (Core, Starter, etc) into an array of GFM strings
  # For example:
  # #### [Ultimate](https://about.gitlab.com/pricing/ultimate/)
  def render_tier(tier, stages, is_ee:)
    description = []

    if is_ee
      # Only generate tier headings for EE, since FOSS will
      # only ever have content from one tier (Core)

      if tier == CORE
        # There is no pricing page for Core, so just list the heading in plain text

        description << "#### #{tier.humanize}\n"
      elsif tier != OTHER
        # For paid tiers, link to the appropriate pricing page

        description << "#### [#{tier.humanize}](https://about.gitlab.com/pricing/#{tier}/)\n"
      end
    end

    stages.each do |stage, highlights|
      description.concat(render_stage(stage, highlights))
    end

    description
  end

  # Renders a stage (Manage, Plan, Create, etc.) into an array of GFM strings
  # For example:
  # ##### [Release](https://about.gitlab.com/stages-devops-lifecycle/release/)
  def render_stage(stage, highlights)
    description = []

    description << "##### [#{stage.humanize}](https://about.gitlab.com/stages-devops-lifecycle/#{stage}/)\n" unless stage === OTHER

    highlights.each do |highlight|
      description.concat(render_highlight(highlight))
    end

    description << "\n"
  end

  # Renders a feature highlight into an array of GFM strings
  # For example:
  # - [Associate milestones with a release](https://docs.gitlab.com/ee/user/project/releases/#releases-associated-with-milestones): `Release Orchestration`
  def render_highlight(highlight)
    list_item = highlight.link ? "- [#{highlight.title}](#{highlight.link})" : "- #{highlight.title}"

    # If the highlight includes "categories", list them after
    # the link, surrounded with backticks (`)
    list_item += ": #{highlight.categories.map { |c| "`#{c}`" }.join(', ')}" unless highlight.categories.blank?

    [list_item]
  end

  # Returns the tier of a highlight as a string
  # If no matching tier is found, "other" is returned
  def get_highlight_tier(highlight)
    return CORE if highlight.tier.include?(CORE)
    return STARTER if highlight.tier.include?(STARTER)
    return PREMIUM if highlight.tier.include?(PREMIUM)
    return ULTIMATE if highlight.tier.include?(ULTIMATE)

    OTHER
  end

  # Returns the stage of a highlight as a string
  # If no matching stage is found, "other" is returned
  def get_highlight_stage(highlight)
    return highlight.stage if STAGES.include?(highlight.stage)

    OTHER
  end

  # Converts a version number like "12.9" into a tag name like "v12.9.0-ee"
  # (or "v12.9.0" is is_ee is false)
  def version_number_to_tag_name(version_number, is_ee:)
    version_segments = Gem::Version.create(version_number).canonical_segments
    version_segments = [
      version_segments[0] || 0,
      version_segments[1] || 0,
      version_segments[2] || 0
    ]

    "v#{version_segments.join('.')}#{is_ee ? '-ee' : ''}"
  end
end
